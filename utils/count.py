#utility to count the number of objects in an image
import numpy as np
import matplotlib.pyplot as pylab 
import mahotas as mh
import cv2

image = cv2.imread('../images/test.png', 0) 

filtered = mh.gaussian_filter(image, 10) 
result = filtered.astype('uint8') 
rmax = mh.regmax(result) 

pylab.imshow(mh.overlay(image, rmax))
pylab.show()

labeled, nr_objects = mh.label(rmax)
print ('Number of nucelei',format(nr_objects))

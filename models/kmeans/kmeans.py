import cv2
import numpy as np
import os
TEST_IMG_DIR = '../../images'

TEST_IMG_DIR = '/Users/robertovazquezlucerga/Dropbox (MIT)/_MIT courses/15.071 Analytics Edge/Project/ff599c7301daa1f783924ac8cbe3ce7b42878f15a39c2d19659189951f540f48/images'

NUM_CLUSTERS = 2


fname = os.listdir(TEST_IMG_DIR)[0]
fname = TEST_IMG_DIR+'/'+fname
img = cv2.imread(fname)
print (os.listdir(TEST_IMG_DIR)[0])
IMG = img.reshape((-1,3))

IMG = np.float32(IMG)
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)

ret, label, center = cv2.kmeans(IMG, NUM_CLUSTERS,None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)

center = np.uint8(center)
res = center[label.flatten()]

res2 = res.reshape(img.shape)

gray1 = cv2.cvtColor(res2, cv2.COLOR_BGR2GRAY)
cnts = cv2.findContours(gray1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
print (len(cnts[0]))
#for c in cnts[0]:
 #   print (cv2.contourArea(c))


#cv2.drawContours(res2, cnts, -1, (0,255,0), 3)

#cv2.imshow('img',gray1)
'''cv2.imshow('res2', res2)'''
cv2.waitKey(0)
cv2.destroyAllWindows()



